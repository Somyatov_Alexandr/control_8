import React, {Component} from 'react';
import axios from 'axios';
import QuoteItem from "../../components/QuoteItem/QuoteItem";
import Loading from "../../components/UI/Loading/Loading";



class QuotesList extends Component {
  state = {
    quotes: [],
    loading: false
  };

  componentDidMount(){
    this.setState({loading: true});
    axios.get('/quotes.json').then(response =>{
      const quotes = [];

      for (let key in response.data) {
        quotes.push({...response.data[key], id: key});
      }

      this.setState({quotes, loading: false});
    })
  }

  removeQuote = id => {
    let quotes = [...this.state.quotes];
    const index = quotes.findIndex(item => item.id === id);
    if (index !== -1) {
      quotes.splice(index, 1);
    }
    this.setState({quotes});

    axios.delete(`/quotes/${id}.json`).finally(() => {
      this.props.history.push('/');
    });
  };


  render () {
    if (!this.state.loading) {
      return (

          <div className="quotes__list">
            {this.state.quotes.map(quote => (
                <QuoteItem
                    key={quote.id}
                    text={quote.text}
                    author={quote.author}
                    id={quote.id}
                    remove={this.removeQuote}
                />
            ))}
          </div>

      );
    } else {
      return <Loading/>
    }
  }
}

export default QuotesList;