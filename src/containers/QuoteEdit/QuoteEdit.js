import React, {Component} from 'react';
import axios from 'axios';

class QuoteEdit extends Component {
  state = {
    quote: {
      author: '',
      text: '',
      category: ''
    },
    loading: false
  };

  changeQuoteHandler = (event) => {
    const quote = {...this.state.quote};
    const key = event.target.name;
    const value = event.target.value;
    quote[key] = value;
    this.setState({quote});
  };

  componentDidMount(){
    const id = this.props.match.params.id;

    axios.get(`/quotes/${id}.json`).then(response =>{
      this.setState({quote: response.data})
    })
  };

  editQuoteHandler = (event) => {
    event.preventDefault();
    this.setState({loading: true});
    const id = this.props.match.params.id;
    const updateQuote = {
      author: this.state.quote.author,
      text: this.state.quote.text,
    };
    axios.patch(`/quotes/${id}.json`, updateQuote).then(() => {
      this.setState({loading: false});
    }).finally(() => {
      this.props.history.push(`/`);
    });
  };


  render () {
    const category = this.props.categories.map(category => (
        <option key={category.id} value={category.id}>{category.title}</option>
    ));
    console.log(this.state.quote);
    return (
        <div className="quote-add plank">
          <h1 className="heading">Edit quote</h1>
          <form className="quote-add__form">
            <div className="form-group">
              <label className="quote__label">Category</label>
              <select
                  className="form-control"
                  name="category" value={this.state.quote.category}
                  onChange={this.changeQuoteHandler}>
                {category}
              </select>
            </div>
            <div className="form-group">
              <label htmlFor="author">Author</label>
              <input
                  className="form-control"
                  type="text"
                  id="author"
                  name="author"
                  value={this.state.quote.author}
                  onChange={this.changeQuoteHandler}
              />
            </div>
            <div className="form-group">
              <label htmlFor="quoteText">Quote text</label>
              <textarea
                  className="form-control"
                  id="quoteText"
                  name="text"
                  value={this.state.quote.text}
                  onChange={this.changeQuoteHandler}
              />
            </div>
            <button className="btn" onClick={this.editQuoteHandler}>Save</button>
          </form>
        </div>
    );
  }
}

export default QuoteEdit;