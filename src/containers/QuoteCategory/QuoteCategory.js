import React, {Component} from 'react';
import axios from 'axios';
import QuoteItem from "../../components/QuoteItem/QuoteItem";
import Loading from "../../components/UI/Loading/Loading";


class QuoteCategory extends Component {
  state = {
    quotes: [],
    category: '',
    loading: false
  };

  categoryFilter(category){
    this.setState({loading: true});
    axios.get(`/quotes.json?orderBy="category"&equalTo="${category}"`).then(response =>{
      const quotes = [];

      for (let key in response.data) {
        quotes.push({...response.data[key], id: key});
      }

      this.setState({quotes, category, loading: false});
    })
  }

  componentDidMount(){
    const category = this.props.match.params.category;
    this.setState({loading: true});
    this.categoryFilter(category);
  }

  componentDidUpdate (prevProps, prevState){
    let category = this.props.match.params.category;
    console.log(category, this.state.category);

    if (category !== this.state.category && !this.state.loading) {
      this.categoryFilter(category);
    }
  }

  removeQuote = id => {
    axios.delete(`/quotes/${id}.json`).finally(() => {
      this.props.history.push('/');
    });
  };

  render () {
    if (!this.state.loading) {
      return (
          <div className="quotes__list">
            {this.state.quotes.map(quote => (
                <QuoteItem
                    key={quote.id}
                    text={quote.text}
                    author={quote.author}
                    id={quote.id}
                    remove={this.removeQuote}
                />
            ))}
          </div>
      );
    } else {
      return <Loading/>
    }
  }
}

export default QuoteCategory;