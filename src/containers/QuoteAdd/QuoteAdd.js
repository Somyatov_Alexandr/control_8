import React, {Component} from 'react';
import './QuoteAdd.css';
import axios from 'axios';
import Loading from "../../components/UI/Loading/Loading";


class QuoteAdd extends Component {
  state = {
    quote: {
      author: '',
      category: 'star-wars',
      text: ''
    },
    loading: false
  };

  changeQuoteHandler = (event) => {
    const quote = {...this.state.quote};
    const key = event.target.name;
    const value = event.target.value;
    quote[key] = value;
    this.setState({quote});
  };

  addQuoteHandler = (event) => {
    event.preventDefault();
    this.setState({loading: true});
    axios.post('/quotes.json', this.state.quote).then(() => {
      this.setState({loading: false});
    }).finally(() => {
      this.props.history.push('/');
      // console.log('yes')
    });
  };

  render () {
    const category = this.props.categories.map(category => (
        <option key={category.id} value={category.id}>{category.title}</option>
    ));
    if (!this.state.loading) {
      return (
          <div className="quote-add plank">
            <h1 className="heading">Submit new quote</h1>
            <form className="quote-add__form">
              <div className="form-group">
                <label className="quote__label">Category</label>
                <select className="form-control" name="category" value={this.state.category}
                        onChange={this.changeQuoteHandler}>
                  {category}
                </select>
              </div>
              <div className="form-group">
                <label htmlFor="author">Author</label>
                <input
                    className="form-control"
                    type="text"
                    id="author"
                    name="author"
                    onChange={this.changeQuoteHandler}
                />
              </div>
              <div className="form-group">
                <label htmlFor="quoteText">Quote text</label>
                <textarea
                    className="form-control"
                    id="quoteText"
                    name="text"
                    onChange={this.changeQuoteHandler}
                />
              </div>
              <button className="btn" onClick={this.addQuoteHandler}>Save</button>
            </form>
          </div>
      );
    } else {
      return <Loading/>
    }
  }
}

export default QuoteAdd;