import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import QuotesList from "./containers/QuotesList/QuotesList";
import QuoteAdd from "./containers/QuoteAdd/QuoteAdd";
import QuoteCategory from "./containers/QuoteCategory/QuoteCategory";
import CategoryNav from "./components/CategoryNav/CategoryNav";
import QuoteEdit from "./containers/QuoteEdit/QuoteEdit";
import Header from "./components/Header/Header";

class App extends Component {
  categories = [
    {title: 'Star Wars', id: 'star-wars'},
    {title: 'Famous people', id: 'famous-people'},
    {title: 'Humor', id: 'humor'},
    {title: 'Writers', id: 'writers'},
    {title: 'Motivational', id: 'motivational'}
  ];
  render() {
    return (
        <div className="wrapper">
          <Header/>
          <div className="main">
            <CategoryNav categories={this.categories}/>
            <Switch>
              <Route path="/" exact render={props => (
                  <QuotesList
                      categories={this.categories}
                      {...props}
                  />
              )}/>
              <Route path="/add" exact render={props => (
                  <QuoteAdd
                      categories={this.categories}
                      {...props}
                  />
              )}/>
              <Route path="/quotes/:category" exact render={props => (
                  <QuoteCategory
                      categories={this.categories}
                      {...props}
                  />
              )}/>
              <Route path="/quote/edit/:id" exact render={props => (
                  <QuoteEdit
                      categories={this.categories}
                      {...props}
                  />
              )}/>
              <Route render={() => <h1>Not found</h1>}/>
            </Switch>
          </div>
        </div>
    );
  }
}

export default App;
