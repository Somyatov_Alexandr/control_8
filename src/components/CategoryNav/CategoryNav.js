import React from 'react';
import './CategoryNav.css';
import {NavLink} from "react-router-dom";

const CategoryNav = props => {
  const categoryNav = props.categories.map(category => (
      <li key={category.id} className="quote-nav__item">
        <NavLink className="quote-nav__link" to={'/quotes/' + category.id} exact>{category.title}</NavLink>
      </li>
  ));
  return (
      <nav className="quotes-nav">
        <h3 className="heading category__heading">Category</h3>
        <menu className="quote-nav__list">
          <li className="quote-nav__item">
            <NavLink className="quote-nav__link" to={'/quotes/all'} exact>All</NavLink>
          </li>
          {categoryNav}
        </menu>
      </nav>
  );
};


export default CategoryNav;