import React from 'react';
import {NavLink} from "react-router-dom";

const Header = () => {
  return (
      <header className="header">
        <div className="logo"><img src="./img/logo.png" alt="quote"/></div>
        <menu className="top-nav">
          <li className="top-nav__item">
            <NavLink className="top-nav__link" to="/" exact>Quotes</NavLink>
          </li>
          <li className="top-nav__item">
            <NavLink className="top-nav__link" to="/add" exact>Submit new quote</NavLink>
          </li>
        </menu>
      </header>
  );
};

export default Header;