import React from 'react';
import './Loading.css';

const Loading = () => {
  return (
      <div className="cssload-container">
        <div className="cssload-loading"><i/><i/></div>
      </div>
  );
};

export default Loading;