import React from 'react';
import './QuoteItem.css';
import {Link} from "react-router-dom";

const QuoteItem = props => {

    return (
        <div className="quote__item">
          <div className="quote__text">{props.text}</div>
          <div className="quote__author">said: <span>{props.author}</span></div>
          <div className="quote__controls">
            <Link className="btn btn-small btn-change" to={`/quote/edit/${props.id}`}>Edit</Link>
            <button className="btn btn-small btn-danger" onClick={() => props.remove(props.id)}>Remove</button>
          </div>
        </div>
    );
};

export default QuoteItem;